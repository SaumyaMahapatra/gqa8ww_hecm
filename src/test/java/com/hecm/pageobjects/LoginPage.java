package com.hecm.pageobjects;

import java.io.IOException;

import com.hecm.element.ObjectRepoReader;
import com.hecm.stepdefinitions.base.BaseStepDef;
import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;

public class LoginPage {
	
	

	public void login(String usertype) throws IOException {

		// login by user in chrome
		System.out.println("i am logging in");
		if ("Chrome".equalsIgnoreCase(AppDriver.getBrowserName())) {

			try {
				Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\autoITScripts\\chromeLogins\\" + "Login_"
						+ usertype + "Chrome" + ".exe");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		// login by user in firefox
		else if ("Firefox".equalsIgnoreCase(AppDriver.getBrowserName())) {

			Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\autoITScripts\\firefoxLogins\\" + "Login_"
					+ usertype + "Firefox" + ".exe");

		}

	}
	
	//Explicitly closing the driver session

	public void logout() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		AppDriver.closeDriver();
	}

	

	
}
