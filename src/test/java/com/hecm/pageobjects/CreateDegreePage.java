package com.hecm.pageobjects;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;
import com.hecm.utils.Utils;

public class CreateDegreePage {

	// enter the mandatory fields in degree page to create a degree

	public void fillDegreeDetails() {

		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.degree_duration.dropdown"), 10);

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.discipline1.checkbox")).click();
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.discipline2.checkbox")).click();
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.coursecode.textbox"))
				.sendKeys(RandomStringUtils.randomAlphanumeric(4));
		WebElement degreeName = Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.degreename.textbox"));
		degreeName.sendKeys(RandomStringUtils.randomAlphabetic(5));

		// Copying the DegreeName
		degreeName.sendKeys(Keys.CONTROL + "a");
		degreeName.sendKeys(Keys.CONTROL + "c");
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.intake_date.radiobutton")).click();

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.active_progression_degree.checkbox")).click();
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.UCAS_code.textbox")).sendKeys("Ucas");
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.UCAS_code.textbox"))
				.sendKeys(RandomStringUtils.randomNumeric(2));
		// Add a preparation course

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.addnewpreparation.link")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.preparation_gpavalue.textbox"))
				.sendKeys(RandomStringUtils.randomNumeric(2));

		for (int i = 1; i <= 3; i++) {
			AppVariables.APP_DRIVER
					.findElement(By
							.xpath("//div[div[input[@name='[0].GPA']]]/following-sibling::div[" + i + "]/div[2]/input"))
					.sendKeys(RandomStringUtils.randomNumeric(2));
		}

	}

	// Saving the Degree
	public void saveDegree() {
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.save_degree.button")).click();
	}

	// verifying the reason for update dropdown values
	public void verifyReasonForUpdate() {

		if (Utils.isElementPresent(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.update_reason.dropdown"))) {

			// expected values for reason for Update field
			String[] reasonUpdateExpected = { "Degree discontinued", "Degree title change",
					"English language requirement change", "Academic requirements change", "Other", "Fees updated",
					"Change in study requirement", "Change in module requirement", "Change in ATAS",
					"Change of College Pathway" };

			Select selectElementReasonUpdate = Utils.getSelectElement(AppVariables.APP_DRIVER,
					AppVariables.OBJECT_REPO.getLocator("createdegreepage.update_reason.dropdown"));

			// retrieving the actual values for reason for update
			List<WebElement> listReasonUpdate = selectElementReasonUpdate.getOptions();
			boolean match = true;
			for (WebElement actualReasonUpdate : listReasonUpdate) {
				if (!match) {
					break;
				}
				match = false;
				for (int i = 0; i < reasonUpdateExpected.length; i++) {

					if (actualReasonUpdate.getText().equals(reasonUpdateExpected[i])) {
						match = true;
						break;
					}
				}

			}
			if (match) {
				System.out.println(" Reason for update dropdown match");
			} else {
				System.out.println("Reason for update dropdown doesnot match");
			}
		} else {
			System.out.println("createdegreepage.update_reason.dropdown element not found");
		}

	}

	// adding comment on setting the reason for update

	public void commentsForChangeinDegree() {
		Utils.isElementPresent(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.updatecomments.text"));
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.updatecomments.text"))
				.sendKeys(RandomStringUtils.randomAlphabetic(10));
	}

	// seting the reason for update

	public void setReasonforUpdate() {
		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.update_reason.dropdown"), 15);
		Select selectElementReasonUpdate = Utils.getSelectElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("createdegreepage.update_reason.dropdown"));
		selectElementReasonUpdate.selectByIndex(7);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
