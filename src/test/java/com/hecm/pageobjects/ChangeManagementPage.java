package com.hecm.pageobjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.hecm.utils.AppVariables;
import com.hecm.utils.Utils;
import com.sun.jna.platform.win32.ShellAPI.APPBARDATA;

public class ChangeManagementPage {
	
	//searching for degree 

	public void searchDegree() throws InterruptedException {

		Utils.isElementPresent(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.degree_search_box.texbox"));

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.degree_search_box.texbox"))
				.sendKeys(Keys.CONTROL + "v");
		Thread.sleep(5000);

	}
	
	//setting the degreestatus and sales team status
	
	public void setDegreeStatusSalesTeamStatus(String degreeStatus, String salesteamoption) {

		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.readyforclass_upgrade.dropdown"), 10);

		Select readyforclassdropdown = Utils.getSelectElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.readyforclass_upgrade.dropdown"));

		readyforclassdropdown.selectByVisibleText(degreeStatus);

		Select salesteamdropdown = Utils.getSelectElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.salesTeam_Informed.dropdown"));

		salesteamdropdown.selectByVisibleText(salesteamoption);

	}
	
	//clicking on save and submit button

	public void saveDetailsInClass() throws InterruptedException {

		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.Save_button.button")).click();
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.submit.button")).click();
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.submitselectedchanges.button")).click();

		Thread.sleep(7000);
	}
	
	//verifying the success message after submission to class

	public void verifysuccesfulSubmitOnClass() {

		WebElement classsubmitstatus = null;

		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.updateclassstatus.div"), 20);
		classsubmitstatus = Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("changemanagementpage.updateclassstatus.div"));

		if (classsubmitstatus.getText().contains("Success")) {
			System.out.println("Class submission is succesful with the status as: " + classsubmitstatus.getText());
		}
		else
		{
			System.out.println("Class submission failure");
		}
	}

	//verify the email gets triggered with new coloumns added
	
	public void verifyEmail() {

		System.out.println(
				" Email should contain the additional field: College,Degree,User,Reason,Detail,ApprovalSpecification,University name, date of change made by college editor,name of change manager that approved the change,date that change manager approved the change");

	}

	

}
