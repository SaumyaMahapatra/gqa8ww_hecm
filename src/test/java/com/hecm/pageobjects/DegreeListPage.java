package com.hecm.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;
import com.hecm.utils.Utils;



public class DegreeListPage {
	
	//verify the presence of degree created

	@SuppressWarnings("deprecation")
	public void verifydegreeList() throws InterruptedException {
		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("degreelistpage.degree_search_box.textbox"), 10);
		Thread.sleep(5000);
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("degreelistpage.degree_search_box.textbox"))
				.sendKeys(Keys.CONTROL + "v");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		

        //verify  whether degree get retrieved
		
		Utils.isElementPresent(AppVariables.APP_DRIVER,AppVariables.OBJECT_REPO.getLocator("degreelistpage.degree_search_result.div"));
	String degreeExpected=Utils.getWebElement(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("degreelistpage.degree_search_result.div")).getText();
		Assert.assertTrue("Degree not Present",(degreeExpected.contains("Showing 1 to 1 of 1 entries")));
	}
	
  // clicking on edit button
	
	public void editDegree() {
		
		Utils.waitForElementPresence(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("degreelistpage.edit.button"), 10);
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("degreelistpage.edit.button")).click();
		
	}
	
	// verifying whether its degree home page or not

	public void degreeHomePage(String degreehomepagetitle) {
		
		Utils.waitForElementPresence(AppVariables.APP_DRIVER,AppVariables.OBJECT_REPO.getLocator("degreelistpage.home.div"),10);
		String degreehomepagetitleActual=Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("degreelistpage.home.div")).getText();
		Assert.assertTrue(degreehomepagetitle.equals(degreehomepagetitleActual));
		
	}

}
