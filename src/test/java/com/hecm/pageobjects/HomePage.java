package com.hecm.pageobjects;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;
import com.hecm.utils.Utils;

public class HomePage {

	public void verifyTabsHomePage() {
		
		Utils.isElementPresent(AppVariables.APP_DRIVER,AppVariables.OBJECT_REPO.getLocator("homepage.degreelist_tab.link"));

		// Expected HomePage Tabs
		List<String> listtabExpected = new ArrayList<String>();
		listtabExpected.add("Degree list");
		listtabExpected.add("Admin");
		listtabExpected.add("Add New Degree");
		listtabExpected.add("Marketing");
		listtabExpected.add("Change Management");
		listtabExpected.add("Translation");

		// Actual HomePage Tabs retrieving
		List<String> listtabActual = new ArrayList<String>();
		for (int i = 1; i <= listtabExpected.size(); i++) {
			WebElement tabelement = AppVariables.APP_DRIVER.findElement(By.xpath("//ul[@id='menu']/li[" + i + "]/a"));
			listtabActual.add(tabelement.getText());
		}
		for (String str:listtabActual)
		{
			System.out.println("Home page Tabs are: "+str);
		}

		Assert.assertEquals(listtabActual.containsAll(listtabExpected), listtabExpected.containsAll(listtabActual));
	}

	// click on add degree button
	public void addNewDegreeButton() throws InterruptedException {

		// check for the presence of the Create degree button element
		Utils.waitForElementPresence(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("homepage.create_degree.link"), 15);
		if (Utils.isElementPresent(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("homepage.create_degree.link")))
		// Click on create degree button

		{
			WebElement degreeElement = Utils.getWebElement(AppVariables.APP_DRIVER,
					AppVariables.OBJECT_REPO.getLocator("homepage.create_degree.link"));
			degreeElement.click();

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Element is not present");
		}
	}

	// clicking on Admin tab and then click on colleges tab under that

	public void clickAdminAreaAndCollege() {
		Utils.getWebElement(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("homepage.adminarea.link"))
				.click();
		if(Utils.isElementPresent(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("homepage.adminarea_collegecourses_link")))
		{
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("homepage.adminarea_collegecourses_link")).click();
		}
		else
			System.out.println("homepage.adminarea_collegecourses_link not found");
	}
	
	//clicking on Change Management tab
	
	public void clickChangeManagementTab()
	{
		if(Utils.isElementPresent(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("homepage.changemanagementtab.link")))
		{
		Utils.getWebElement(AppVariables.APP_DRIVER,
				AppVariables.OBJECT_REPO.getLocator("homepage.changemanagementtab.link")).click();
		}
	}

}
