package com.hecm.stepdefinitions;

import com.hecm.pageobjects.ChangeManagementPage;
import com.hecm.pageobjects.HomePage;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChangeManagementTestSteps {

	private HomePage homepage = new HomePage();
	private ChangeManagementPage changemanagementpage = new ChangeManagementPage();

	@When("^I go to change management$")
	public void i_go_to_change_management() throws Throwable {

		homepage.clickChangeManagementTab();

	}

	@When("^I search for the degree$")
	public void i_search_for_the_degree() throws Throwable {
		changemanagementpage.searchDegree();
	}

	@When("^I choose to save$")
	public void i_choose_to_save() throws Throwable {
		changemanagementpage.saveDetailsInClass();
	}

	@Then("^I should be able to see a success message to class$")
	public void i_should_be_able_to_see_a_success_message_to_class() throws Throwable {
		changemanagementpage.verifysuccesfulSubmitOnClass();
	}

	@And("^I set the degree status \"([^\"]*)\" and sales team value to \"([^\"]*)\"$")
	public void i_set_degreestatus_salesteamstatus(String degreeStatus, String salesteamoption) {
		changemanagementpage.setDegreeStatusSalesTeamStatus(degreeStatus, salesteamoption);
	}

	@Then("^I should receive an autogenerated email$")
	public void i_should_receive_an_autogenerated_email() throws Throwable {
		changemanagementpage.verifyEmail();
	}

}
