package com.hecm.stepdefinitions;

import java.io.IOException;

import com.hecm.pageobjects.HomePage;
import com.hecm.pageobjects.LoginPage;
import com.hecm.stepdefinitions.base.BaseStepDef;
import com.hecm.utils.AppDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginTestSteps {

	private LoginPage loginpage = new LoginPage();
	private HomePage adminhomepage = new HomePage();
	private BaseStepDef basestepdef =new BaseStepDef();
	

	@Given("^I login as \"([^\"]*)\" user$")
	public void I_am_login_as_an_user(String usertype) throws IOException, InterruptedException {
		loginpage.login(usertype);

	}

	@When("^I login into HECM$")

	public void I_am_login_into_HECM() {
		AppDriver.launchUrl("");
	}

	@Then("^I should see all the tabs Degree list,Admin,Add New Degree,Marketing,Change Management,Translation$")
	public void I_Should_see_all_the_tabs() {
		adminhomepage.verifyTabsHomePage();
	}
	
	@Then("^I log out of hecm$")
	
	public void I_log_out_of_HECM()
	{
		loginpage.logout();
	}
	
	@Given("^I again do set up$")
	
	public void I_again_open_hecm()
	{
		basestepdef.doSetup();
	}

}
