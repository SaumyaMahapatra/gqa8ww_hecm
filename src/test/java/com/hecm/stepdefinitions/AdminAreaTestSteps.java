package com.hecm.stepdefinitions;


import com.hecm.pageobjects.AdminAreaHomePage;
import com.hecm.pageobjects.HomePage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdminAreaTestSteps {
	private HomePage homepage=new HomePage();
	private AdminAreaHomePage adminhomepage=new AdminAreaHomePage();

	
	@And("^I click on colleges and courses under admin tab$")
	public void i_click_on_colleges_and_courses_under_admin_tab() throws Throwable {
		homepage.clickAdminAreaAndCollege();
	   
	}

	@When("^I click on the Add new college$")
	public void i_click_on_the_Add_new_college() throws Throwable {
		adminhomepage.addCollege();
		
	    	}

	@When("^I click on edit new college$")
	public void i_click_on_edit_new_college() throws Throwable {
		adminhomepage.editCollege();
	    
	}
	@Then("^I should be able to see the Class DB dropdown with values  UKP, NAA, ICP$")
	
	public void I_should_able_to_see_classDb_dropdown_Values()
	{
		adminhomepage.verifyClassDbDropdown();
	}
}
