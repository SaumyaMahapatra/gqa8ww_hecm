package com.hecm.utils;

import com.jayway.restassured.RestAssured;
import static org.hamcrest.Matchers.equalTo;

public class RestAssure {

	public static String getAPIPath(){
		return new PropertyReader("/com/hecm/properties/url.properties").readProperty(AppVariables.APP_ENV+".fromPrice.api.path");
	}
	
	public static String getAPIToken(){
		return new PropertyReader("/com/hecm/properties/url.properties").readProperty(AppVariables.APP_ENV+".fromPrice.api.token");
	}
	
	public static String getResponse(String var1, String var2){
				
		RestAssured.baseURI = getAPIPath();

		String apiToken = getAPIToken();
		
		String json = RestAssured.get(var1+var2+apiToken).asString();
		
		return json;
	}
	
	public static void assertResponse(String var1, String var2, String response){
		
		RestAssured.baseURI = getAPIPath();

		String apiToken = getAPIToken();
		
		RestAssured.get(var1+var2+apiToken).then().assertThat().body("responceData",equalTo(response));

	}


}