package com.hecm.utils;

import org.openqa.selenium.WebDriver;

import com.hecm.element.ObjectRepoReader;

public class AppVariables {
	public static WebDriver APP_DRIVER;
	public static String APP_URL;
	public static String APP_BACKEND_URL;
	public static String APP_SECONDARY_URL;
	public static String APP_ENV;
		
	public static String APP_MARKET_KEY = "app_market";

	public static String APP_MARKET_DEFAULT = "";

	public static String APP_ENV_KEY="environment";

	public static ObjectRepoReader OBJECT_REPO;
	
	}
