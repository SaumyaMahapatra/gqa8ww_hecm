QA Framework Skeleton

Follow the following steps to rename the File/Folders to you project and update the file paths

RENAME DIRECTORIES/FILES

1) src/main/java
     com.YOUR_PROJECT.utils

2)  src/test/java
     com.YOUR_PROJECT.dataobjects
     com.YOUR_PROJECT.element
     com.YOUR_PROJECT.stepdefinitions
     com.YOUR_PROJECT.stepdefinitions.base
     com.YOUR_PROJECT.testrunner

3) src/test/resources/com/YOUR_PROJECT


UPDATE LINKS IN CODE

1) src/main/java/com.YOUR_PROJECT.utils/AppDriver.java
     
     Find function “getURL( )"
     Update the path "/com/YOUR_PROJECT/properties/url.properties”

2) src/main/java/com.YOUR_PROJECT.utils/RestAssure.java
     
     Find function "getAPIPath( )"
     
     Update the path "/com/YOUR_PROJECT/properties/url.properties"

     Find function "getAPIToken( )"
     
     Update the path "/com/YOUR_PROJECT/properties/url.properties"

3) src/test/java/com.YOUR_PROJECT.stepdefinitions.base/BaseStepDef.java
     Find function “doSetUp( )"
     
     Update the path "/com/YOUR_PROJECT/properties/ObjectRepository.properties"

4) src/test/java/com.YOUR_PROJECT.testrunner.base/En_TestRunner.java
     
    Update the path  "glue = "com.YOUR_PROJECT.stepdefinitions”,"

5) src/test/java/com.YOUR_PROJECT.testrunner.base/ReRunTestRunner.java
    
    Update the path  "glue = "com.YOUR_PROJECT.stepdefinitions”,"